package com.fahrul.training.microservices.sales.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
public class Sales {
    @Id
    @GeneratedValue(generator = "system-uuid2")
    @GenericGenerator(name = "system-uuid2", strategy = "uuid2")
    private String id;

    @NotNull
    private LocalDateTime transactionTime;

    @NotEmpty @NotNull
    private String salesNumber;

    @NotNull
    @ManyToOne @JoinColumn(name = "id_customer")
    private Customer customer;

}

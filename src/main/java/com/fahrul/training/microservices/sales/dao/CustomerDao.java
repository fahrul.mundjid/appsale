package com.fahrul.training.microservices.sales.dao;

import com.fahrul.training.microservices.sales.entity.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String> {
}
